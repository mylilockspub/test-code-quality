#!/bin/sh

  set -e
  echo "***code quality gate $(ls -l)"
  file=gl-code-quality-report.json
  file1=gl-code-quality-report1.json
  file2=gl-code-quality-report2.json
  diff_file=$CI_PROJECT_DIR/diff.json
  if [ -f $file ]; then
    #echo "**quality report $(ls -l)"
    #more $file
    cp -a $file1 $file2
    cp -a $file $file1
    echo "***code quality gate for diff $(ls -l)"
    #[ diff $file $file2 > $diff_file ] && echo "diff succeed" || echo "diff failed"
    diff $file $file2 > $diff_file 
    if [[ $? != 0 ]]; then 
      echo "*****diff failed****"
    fi
    #echo "script 2: diff status $? and files $(ls -l)"
    #export files=$(ls -l)
    echo "script: diff files $(ls -l)"
    if [ -f $diff_file ] && [ -s $diff_file ]; then
      export add_lines=$(grep -i "^+" $diff_file | wc -l)
      echo "script: addlines $add_lines"
      if [ $add_lines > $CQ_FAIL ]; then exit 1; fi
    else
      add_lines=$(grep -o "Issue" $file1 | wc -l)
      echo "script: reported issues $add_lines"
      if [ $add_lines > $CQ_FAIL ]; then exit 2; fi
    fi
  fi
  #export files=$(ls -l)
  #echo "script: end files $files"
